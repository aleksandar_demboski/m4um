package com.forum.project;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.forum.project.model.Post;
import com.forum.project.model.Topic;
import com.forum.project.rest.HttpPostRequestPost;

public class CreatePostFragment extends Fragment {

    private static final String url = "http://192.168.43.17:8080/4um/mobile/post/create";
    private HttpPostRequestPost task;
    private Topic topic;
    private EditText postNameEditText;
    private EditText postContentEditText;
    private Button postButton;

    public CreatePostFragment() {
    }

    public static CreatePostFragment newInstance(Topic topic) {
        CreatePostFragment fragment = new CreatePostFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.topic = topic;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView  = inflater.inflate(R.layout.fragment_create_post, container, false);
        postNameEditText = (EditText) rootView.findViewById(R.id.post_name_edit_text);
        postContentEditText = (EditText) rootView.findViewById(R.id.post_content_edit_text);
        postButton = (Button) rootView.findViewById(R.id.create_post_button);

        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Post post = new Post();
                post.setName(postNameEditText.getText().toString());
                post.setContent(postContentEditText.getText().toString());
                post.setTopic(topic);
                task = new HttpPostRequestPost(post);
                MainActivity main = (MainActivity) getActivity();
                task.execute(url, main.getCookies());
                main.setPostsFragment(topic);
            }
        });
        return rootView;
    }
}
