package com.forum.project;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.forum.project.model.Topic;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandar on 9/19/2016.
 */
public class TopicsAdapter extends RecyclerView.Adapter<TopicsAdapter.TopicViewHolder> {

    private List<Topic> mDataset;
    private LayoutInflater layoutInflater ;
    private ViewGroup parent;
    private MyClickListener myClickListener;
    private Context context;

    public TopicsAdapter(Context context){
        this.context = context;
        mDataset = new ArrayList<Topic>();
    }

    public TopicsAdapter(List<Topic> topics, Context context){
        this.context = context;
        mDataset = topics;
    }

    public void setTopics(List<Topic> topics){
        mDataset = topics;
    }

    public void setMyClickListener(MyClickListener myClickListener){
        this.myClickListener = myClickListener;
    }

    @Override
    public TopicsAdapter.TopicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        this.parent = parent;
        View v = layoutInflater
                .inflate(R.layout.topic_item_view, this.parent, false);

        RecyclerView.ViewHolder vh = new TopicViewHolder(v);
        return (TopicViewHolder) vh;
    }

    @Override
    public void onBindViewHolder(TopicsAdapter.TopicViewHolder holder, int position) {
        Topic t =  mDataset.get(position);
        holder.topicNameTextView.setText(t.getName());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class TopicViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public View mView;
        public TextView topicNameTextView;

        public TopicViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mView = itemView;
            topicNameTextView = (TextView) itemView.findViewById(R.id.topic_name_text_view);
        }

        @Override
        public void onClick(View view) {
            if(myClickListener != null){
                myClickListener.itemClicked(view, getAdapterPosition());
            }
        }
    }

    public Topic getTopicAt(int i){
        return mDataset.get(i);
    }

    public interface TopicClickListener{
        public void itemClicked(View view, int position);
    }
}

