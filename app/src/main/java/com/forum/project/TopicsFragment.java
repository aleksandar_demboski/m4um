package com.forum.project;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.forum.project.model.Topic;
import com.forum.project.rest.HttpRequestTopicsTask;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;


public class TopicsFragment extends Fragment implements MyClickListener{

    private static final String url = "http://192.168.43.17:8080/4um/mobile/topics";
    private RecyclerView mRecyclerView;
    private FloatingActionButton addTopicButton;
    private TopicsAdapter mAdapter;
    private HttpRequestTopicsTask task;

    public TopicsFragment() {
    }

    public static TopicsFragment newInstance(String param1, String param2) {
        TopicsFragment fragment = new TopicsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.task = new HttpRequestTopicsTask();
        task.execute(url);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_topics, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.topics_recycler);
        mRecyclerView.setHasFixedSize(true);
        addTopicButton = (FloatingActionButton) rootView.findViewById(R.id.add_topic_button);

        addTopicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity main = (MainActivity) getActivity();
                main.setCreateTopicFragment();
            }
        });

        List<Topic> topics = new ArrayList<Topic>();
        try {
            topics = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        mAdapter = new TopicsAdapter(topics, getActivity());
        mAdapter.setMyClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return  rootView;
    }

    @Override
    public void itemClicked(View view, int position) {
        Topic topic =  mAdapter.getTopicAt(position);
        MainActivity main = (MainActivity) getActivity();
        main.setPostsFragment(topic);
    }
}
