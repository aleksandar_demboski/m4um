package com.forum.project;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.forum.project.model.Post;
import com.forum.project.model.Topic;
import com.forum.project.rest.HttpRequestPostsTask;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class PostsFragmen extends Fragment implements MyClickListener{

    private static final String url = "http://192.168.43.17:8080/4um/mobile/posts";
    private static final String COOKIES = "cookies";
    private RecyclerView mRecyclerView;
    private PostsAdapter mAdapter;
    private TextView postsDomainTextView;
    private FloatingActionButton addPostButton;
    private HttpRequestPostsTask task;

    private String cookies;
    private Topic topic;

    public PostsFragmen() {
    }

    public static PostsFragmen newInstance(String cookies, Topic topic) {
        PostsFragmen fragment = new PostsFragmen();
        Bundle args = new Bundle();
        args.putString(COOKIES, cookies);
        fragment.setArguments(args);
        fragment.topic = topic;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.task = new HttpRequestPostsTask();
        if (getArguments() != null) {
            cookies = getArguments().getString(COOKIES);
        }
        if(topic != null)
        task.execute(url + "/" + topic.getId(), cookies);
        else task.execute(url, cookies);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_posts, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.posts_recycler);
        mRecyclerView.setHasFixedSize(true);
        if(topic != null){
            postsDomainTextView = (TextView) rootView.findViewById(R.id.posts_domain_text_view);
            postsDomainTextView.setText(topic.getName());
            postsDomainTextView.setVisibility(View.VISIBLE);
            addPostButton = (FloatingActionButton) rootView.findViewById(R.id.add_post_button);
            addPostButton.setVisibility(View.VISIBLE);
            addPostButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MainActivity main = (MainActivity) getActivity();
                    main.setCreatePostFragment(topic);
                }
            });
        }
        List<Post> posts = new ArrayList<Post>();
            try {
                posts = task.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        mAdapter = new PostsAdapter(posts, getActivity());
        mAdapter.setMyClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return rootView;
    }

    @Override
    public void itemClicked(View view, int position) {
        Post post =  mAdapter.getPostAt(position);
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setCommentsFragment(post);
    }
}
