package com.forum.project;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.forum.project.model.Comment;
import com.forum.project.rest.HttpPostRequestComment;

public class CreateCommentFragment extends Fragment {

    private static final String url = "http://192.168.43.17:8080/4um/mobile/comment/create/";
    private HttpPostRequestComment task;
    private EditText commentContentEditText;
    private Button createCommentButton;
    private long post_id;


    public CreateCommentFragment() {
    }

    public static CreateCommentFragment newInstance(long post_id) {
        CreateCommentFragment fragment = new CreateCommentFragment();
        Bundle args = new Bundle();
        args.putLong("postId", post_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
           post_id = getArguments().getLong("postId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_create_comment, container, false);
        commentContentEditText = (EditText) rootView.findViewById(R.id.comment_content_edit_text);
        createCommentButton = (Button) rootView.findViewById(R.id.create_comment_button);
        createCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Comment comment = new Comment();
                comment.setContent(commentContentEditText.getText().toString());
                task = new HttpPostRequestComment(comment);
                MainActivity mainActivity = (MainActivity) getActivity();
                task.execute(url + post_id, mainActivity.getCookies());
                mainActivity.setPostsFragment();
            }
        });
        return rootView;
    }
}
