package com.forum.project;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.forum.project.model.Comment;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandar on 9/20/2016.
 */
public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentViewHolder> {

    private List<Comment> mDataset;
    private LayoutInflater layoutInflater ;
    private ViewGroup parent;
    private MyClickListener myClickListener;
    private Context context;

    public CommentsAdapter(Context context){
        this.context = context;
        mDataset = new ArrayList<Comment>();
    }

    public CommentsAdapter(List<Comment> comments, Context context){
        this.context = context;
        mDataset = comments;
    }

    public void setComments(List<Comment> comments){
        mDataset = comments;
    }

    @Override
    public CommentsAdapter.CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        this.parent = parent;
        View v = layoutInflater
                .inflate(R.layout.comment_item_view, this.parent, false);

        RecyclerView.ViewHolder vh = new CommentsAdapter.CommentViewHolder(v);
        return (CommentsAdapter.CommentViewHolder) vh;
    }

    @Override
    public void onBindViewHolder(CommentsAdapter.CommentViewHolder holder, int position) {
        Comment c =  mDataset.get(position);
        holder.commnetContentTextView.setText(c.getContent());
    }

    public void setMyClickListener(MyClickListener myClickListener){
        this.myClickListener = myClickListener;
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public View mView;
        public TextView commnetContentTextView;

        public CommentViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            itemView.setOnClickListener(this);
            commnetContentTextView = (TextView) itemView.findViewById(R.id.comment_content_text_view);
        }

        @Override
        public void onClick(View view) {
            if(myClickListener != null){
                myClickListener.itemClicked(view, getAdapterPosition());
            }
        }
    }

    public Comment getCommentAt(int i){
        return mDataset.get(i);
    }
}
