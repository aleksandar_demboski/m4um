package com.forum.project;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.forum.project.model.Post;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandar on 9/19/2016.
 */
public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.PostViewHolder> {

    private List<Post> mDataset;
    private LayoutInflater layoutInflater ;
    private ViewGroup parent;
    private MyClickListener myClickListener;
    private Context context;

    public PostsAdapter(Context context){
        this.context = context;
        mDataset = new ArrayList<Post>();
    }

    public PostsAdapter(List<Post> posts, Context context){
        this.context = context;
        mDataset = posts;
    }

    public void setPosts(List<Post> posts){
        mDataset = posts;
    }

    @Override
    public PostsAdapter.PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(parent.getContext());
        this.parent = parent;
        View v = layoutInflater
                .inflate(R.layout.post_item_view, this.parent, false);

        RecyclerView.ViewHolder vh = new PostViewHolder(v);
        return (PostViewHolder) vh;
    }

    @Override
    public void onBindViewHolder(PostsAdapter.PostViewHolder holder, int position) {
     /*   PostItemViewBinding postItemViewBinding = DataBindingUtil.inflate(layoutInflater,
                R.layout.post_item_view, this.parent, false);
        Post post = mDataset.get(position);
        postItemViewBinding.setPost(post);*/
        Post p =  mDataset.get(position);
        holder.postNameTextView.setText(p.getName());
    }

    public void setMyClickListener(MyClickListener myClickListener){
        this.myClickListener = myClickListener;
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class PostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public View mView;
        public TextView postNameTextView;

        public PostViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            itemView.setOnClickListener(this);
            postNameTextView = (TextView) itemView.findViewById(R.id.post_name_text_view);
        }

        @Override
        public void onClick(View view) {
            if(myClickListener != null){
                myClickListener.itemClicked(view, getAdapterPosition());
            }

        }
    }

    public Post getPostAt(int i){
        return mDataset.get(i);
    }
}
