package com.forum.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")

public class Comment extends BaseEntity {

	private String content;

	private User user;
	private Date creationDate;
	private int upvotes;
	private List<Comment> comments;
	
	public Comment(){setCreationDate();}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public User getUser(){
		return this.user;
	}
	
	public void setUser(User user){
		this.user = user;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate() {
		this.creationDate = new Date();
	}

	public int getUpvotes() {
		return upvotes;
	}

	public void setUpvotes(boolean add) {
		if(add) this.upvotes ++;
		this.upvotes --;
	}

	@Override
	public String toString(){
		return String.format("%s", this.content);
	}

	@JsonIgnore
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
}