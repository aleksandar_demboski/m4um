package com.forum.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")

public class Topic extends BaseEntity{

	private User user;
	private String name;
	private String content;
	private Date creationDate;
	private int upvotes;
	private List<Post> posts;
	private List<User> folowers;
	
	public Topic(){setCreationDate();}
	
	public User getUser(){
		return this.user;
	}
	
	public void setUser(User user){
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate() {
		this.creationDate = new Date();
	}

	public int getUpvotes() {
		return upvotes;
	}

	public void setUpvotes(boolean add) {
		if(add) this.upvotes ++;
		this.upvotes --;
	}

	@JsonIgnore
	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	@JsonIgnore
	public List<User> getFolowers() { 
		return folowers;
	}

	public void setFolowers(List<User> folowers) {
		this.folowers = folowers;
	}
	
	@Override
	public String toString(){
		return String.format("%s\t%s\t%s\n", this.name, this.content, this.user.toString());
	}
}