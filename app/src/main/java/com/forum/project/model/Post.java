package com.forum.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")

public class Post extends BaseEntity {

	private String name;
	private String content;
	private User user;
	private Topic topic;
	private List<Comment> comments;
	private Date creationDate;
	private int upvotes;
		
	public Post(){setCreationDate();}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public Topic getTopic() {
		return this.topic;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}
	
	@JsonIgnore
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	public void setCreationDate(){
		this.creationDate = new Date();
	}
	
	public Date getCreationDate(){
		return this.creationDate;
	}
	
	public int getUpvotes() {
		return upvotes;
	}

	public void setUpvotes(boolean add) {
		if(add) this.upvotes ++;
		this.upvotes --;
	}
	
	@Override
	public String toString(){
		return String.format("%s\t%sn", this.name, this.content);
	}
}