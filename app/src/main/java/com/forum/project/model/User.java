package com.forum.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

@SuppressWarnings("serial")

public class User extends BaseEntity {

	private String name;
	private String username;
	private String password;
	private String email;
	private List<Post> posts;
	private List<Topic> topics;
	private List<Topic> following;
	
	public User(){}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@JsonIgnore
	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}
	
	@JsonIgnore
	public List<Topic> getTopics() {
		return topics;
	}

	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}
	
	@JsonIgnore
	public List<Topic> getFollowing() {
		return this.following;
	}

	public void setFollowing(List<Topic> following) {
		this.following = following;
	}
	
	@Override
	public String toString(){
		return String.format("%s\t%s\t%s\t%s\n", this.name, this.username, this.email, this.password );
	}
}