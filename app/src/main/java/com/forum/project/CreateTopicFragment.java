package com.forum.project;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.forum.project.model.Topic;
import com.forum.project.rest.HttpPostRequestTopic;
import com.forum.project.rest.HttpRequestTopicsTask;

public class CreateTopicFragment extends Fragment {

    private static final String url = "http://192.168.43.17:8080/4um/mobile/topic/create";
    private HttpPostRequestTopic task;
    private EditText topicNameEditText;
    private Button addTopicButton;

    public CreateTopicFragment() {
    }

    public static CreateTopicFragment newInstance() {
        CreateTopicFragment fragment = new CreateTopicFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_create_topic, container, false);
        topicNameEditText = (EditText) rootView.findViewById(R.id.topic_name_edit_text);
        addTopicButton = (Button) rootView.findViewById(R.id.create_topic_button);

        addTopicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Topic topic = new Topic();
                topic.setName(topicNameEditText.getText().toString());
                Log.d("MSG", topic.getName());
                task = new HttpPostRequestTopic(topic);
                MainActivity mainActivity = (MainActivity) getActivity();
                task.execute(url, mainActivity.getCookies());
                mainActivity.setTopicsFragment();
            }
        });
        return rootView;
    }
}