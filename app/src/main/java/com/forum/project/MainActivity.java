package com.forum.project;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.forum.project.model.Post;
import com.forum.project.model.Topic;

import static com.forum.project.PostsFragmen.newInstance;

public class MainActivity extends AppCompatActivity {

    public static final String SESSION_TOKEN = "sessionToken";
    private String cookies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getCookies();
        inject();
    }


    public String getCookies(){
        SharedPreferences settings = getPreferences(MODE_PRIVATE);
        return settings.getString(SESSION_TOKEN, null);
    }

    public void setCookies(String cookies){
        SharedPreferences settings = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(SESSION_TOKEN, cookies);
        editor.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void inject() {
        setPostsFragment();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == R.id.action_posts){
            setPostsFragment();
            return true;
        }else if(id == R.id.action_topics){
            setTopicsFragment();
            return true;
        }else if(id == R.id.action_login){
            setLoginFragment();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setLoginFragment() {
        LoginFragment f = new LoginFragment();
        swapFragment(f);
    }

    public void setPostsFragment(){
        PostsFragmen f = new PostsFragmen();
        swapFragment(f);
    }

    public void setPostsFragment(Topic topic){
        PostsFragmen f = newInstance(cookies, topic);
        swapFragment(f);
    }

    public void setTopicsFragment(){
        TopicsFragment f = new TopicsFragment();
        swapFragment(f);
    }

    public void setCreatePostFragment(Topic topic){
        CreatePostFragment f = CreatePostFragment.newInstance(topic);
        swapFragment(f);
    }

    public void setCreateTopicFragment(){
        CreateTopicFragment f = CreateTopicFragment.newInstance();
        swapFragment(f);
    }

    public void setCommentsFragment(Post post){
        CommentsFragment f = CommentsFragment.newInstance(post);
        swapFragment(f);

    }

    public void setCreateCommentFragment(long post_id){
        CreateCommentFragment f = new CreateCommentFragment().newInstance(post_id);
        swapFragment(f);
    }

    public void swapFragment(Fragment f){
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.parent_container,f);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}