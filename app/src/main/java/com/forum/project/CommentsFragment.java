package com.forum.project;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.forum.project.model.Comment;
import com.forum.project.model.Post;
import com.forum.project.rest.HttpRequestCommentsTask;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class CommentsFragment extends Fragment implements MyClickListener{

    private static final String url = "http://192.168.43.17:8080/4um/mobile/post/";
    private RecyclerView mRecyclerView;
    private CommentsAdapter mAdapter;
    private TextView commentsDomainTextView;
    private FloatingActionButton addCommentButton;
    private HttpRequestCommentsTask task;
    private Post post;

    public CommentsFragment() {
    }

    public static CommentsFragment newInstance(Post post) {
        CommentsFragment fragment = new CommentsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.post = post;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.task = new HttpRequestCommentsTask();
        if (getArguments() != null) {
        }
        task = new HttpRequestCommentsTask();
        task.execute(url + post.getId() + "/comments");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_comments, container, false);
        commentsDomainTextView = (TextView) rootView.findViewById(R.id.comments_domain_text_view);
        commentsDomainTextView.setText(post.getName());
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.comments_recycler);
        mRecyclerView.setHasFixedSize(true);
        addCommentButton = (FloatingActionButton) rootView.findViewById(R.id.add_comment_button);
        addCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.setCreateCommentFragment(post.getId());
            }
        });
        List<Comment> comments = new ArrayList<Comment>();
        try {
            comments = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        mAdapter = new CommentsAdapter(comments, getActivity());
        mAdapter.setMyClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return rootView;
    }

    @Override
    public void itemClicked(View view, int position) {
        //TODO
    }
}
