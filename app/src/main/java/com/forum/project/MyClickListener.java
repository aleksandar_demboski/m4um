package com.forum.project;

import android.view.View;

/**
 * Created by Aleksandar on 9/19/2016.
 */
public interface MyClickListener {
    public void itemClicked(View view, int position);
}
