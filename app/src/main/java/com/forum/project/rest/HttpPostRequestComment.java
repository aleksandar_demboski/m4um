package com.forum.project.rest;

import android.os.AsyncTask;
import com.forum.project.model.Comment;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Aleksandar on 9/21/2016.
 */
public class HttpPostRequestComment  extends AsyncTask<String, Void, Comment> {

    private Comment comment;

    public HttpPostRequestComment(Comment comment){
        super();
        this.comment = comment;
    }

    @Override
    protected Comment doInBackground(String... params) {
        String url = params[0];
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpHeaders requestHeaders = new HttpHeaders();
        if(params[1] != null) requestHeaders.add("Cookie", params[1]);
        HttpEntity<Comment> request  = new HttpEntity<>(this.comment,requestHeaders);
        ResponseEntity<Comment> responseEntity  = restTemplate.exchange(url, HttpMethod.POST, request, Comment.class);
        responseEntity.getStatusCode();
        if(responseEntity.getStatusCode() == HttpStatus.OK){
            Comment result = responseEntity.getBody();
            return result;
        }
        return null;
    }
}
