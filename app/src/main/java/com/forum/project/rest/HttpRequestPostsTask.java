package com.forum.project.rest;

import android.os.AsyncTask;
import com.forum.project.model.Post;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Aleksandar on 8/12/2016.
 */
public class HttpRequestPostsTask extends AsyncTask<String, Void, ArrayList<Post>> {

    @Override
    protected ArrayList<Post> doInBackground(String... params) {
        String url = params[0];
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        /*ResponseEntity<Post[]> responseEntity = restTemplate.getForEntity(url, Post[].class);*/
        HttpHeaders requestHeaders = new HttpHeaders();
        if(params[1] != null) requestHeaders.add("Cookie", params[1]);
        HttpEntity requestEntity = new HttpEntity(null, requestHeaders);
        ResponseEntity<Post[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Post[].class);
        //responseEntity.getStatusCode();
        Post[] items = responseEntity.getBody();
        ArrayList<Post> list = new ArrayList<Post>(Arrays.asList(items));

        return list;
    }
}
