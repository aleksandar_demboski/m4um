package com.forum.project.rest;

import android.os.AsyncTask;
import com.forum.project.model.Comment;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Aleksandar on 8/15/2016.
 */
public class HttpRequestCommentsTask extends AsyncTask<String, Void, ArrayList<Comment>> {

    @Override
    protected ArrayList<Comment> doInBackground(String... params) {
        String url = params[0];
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        ResponseEntity<Comment[]> responseEntity = restTemplate.getForEntity(url, Comment[].class);
        Comment[] items = responseEntity.getBody();
        ArrayList<Comment> list = new ArrayList<Comment>(Arrays.asList(items));
        MediaType contentType = responseEntity.getHeaders().getContentType();
        HttpStatus statusCode = responseEntity.getStatusCode();
       /* HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Cookie", "dsasd");
        HttpEntity requestEntity = new HttpEntity(null, requestHeaders);
        responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Comment[].class);*/
        return list;
    }
}