package com.forum.project.rest;

import android.os.AsyncTask;
import com.forum.project.model.Topic;
import com.forum.project.model.User;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Aleksandar on 9/21/2016.
 */
public class HttpPostRequestTopic extends AsyncTask<String, Void, Topic> {

    private Topic topic;

    public HttpPostRequestTopic(Topic topic){
        super();
        this.topic = topic;
    }

    @Override
    protected Topic doInBackground(String... params) {

        //User user = new User();
        /*Topic t = new Topic();
        t.setName("snewteseotas");*/
        //t.setUser(user);

        String url = params[0];
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpHeaders requestHeaders = new HttpHeaders();
        if(params[1] != null) requestHeaders.add("Cookie", params[1]);

        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Topic> request  = new HttpEntity<>(topic, requestHeaders);

        ResponseEntity<Topic> responseEntity = restTemplate.exchange(url, HttpMethod.POST, request, Topic.class);

        if(responseEntity.getStatusCode() == HttpStatus.OK){
            Topic result = responseEntity.getBody();
            return result;
        }
        return null;
    }
}
