package com.forum.project.rest;


import android.os.AsyncTask;
import com.forum.project.model.Post;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Aleksandar on 9/20/2016.
 */
public class HttpPostRequestPost  extends AsyncTask<String, Void, Post> {

    private Post post;

    public HttpPostRequestPost(Post post){
        super();
        this.post = post;
    }

    @Override
    protected Post doInBackground(String... params) {

      /*  User user = new User();
        user.setId(1);
        user.setUsername("seth");
        user.setPassword("pass");
        user.setEmail("a.demboski@yahoo.com");
        user.setName("acko");

        Topic topic = new Topic();
        topic.setId(1);
        topic.setName("Dota 2");
        topic.setUser(user);

        Post post1 = new Post();
        post1.setName("andoirdPost1");
        post1.setContent("andoirdContent");
        post1.setTopic(topic);
        post1.setUser(user);*/

        String url = params[0];
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        HttpHeaders requestHeaders = new HttpHeaders();
        if(params[1] != null) requestHeaders.add("Cookie", params[1]);
       /* ObjectMapper mapper = new ObjectMapper();
        JSONObject object;
        mapper.writer(object)
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);*/
        HttpEntity<Post> request  = new HttpEntity<>(this.post,requestHeaders);
        ResponseEntity<Post> responseEntity  = restTemplate.exchange(url, HttpMethod.POST, request, Post.class);
        responseEntity.getStatusCode();

        if(responseEntity.getStatusCode() == HttpStatus.OK){
            Post result = responseEntity.getBody();
            return result;
        }
        return null;
    }
}
