package com.forum.project.rest;

import android.os.AsyncTask;
import com.forum.project.model.Topic;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Aleksandar on 9/7/2016.
 */
public class HttpRequestTopicsTask extends AsyncTask<String, Void, ArrayList<Topic>> {

    @Override
    protected ArrayList<Topic> doInBackground(String... params) {
        String url = params[0];
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        ResponseEntity<Topic[]> responseEntity = restTemplate.getForEntity(url, Topic[].class);
        Topic[] items = responseEntity.getBody();
        ArrayList<Topic> list = new ArrayList<Topic>(Arrays.asList(items));



        return list;
    }
}
